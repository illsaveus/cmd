﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TBB;
public class UI_Settings_Controller : MonoBehaviour
{
    public DragAndDrop dndPlayer;
    public Slider movementSlider, cameraSlider;
    
    public void SaveChanges()
    {
        dndPlayer.ChangePlayerSettings(movementSlider.value, cameraSlider.value);
    }

    public void UpdateSliders()
    {
        movementSlider.value = dndPlayer.GetPlayerPrefs("PlayerMovement");
        cameraSlider.value = dndPlayer.GetPlayerPrefs("CameraMovement");
    }
}
