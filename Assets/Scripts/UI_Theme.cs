﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


[CreateAssetMenu(fileName = "New UI Theme", menuName = "UI Theme")]
[System.Serializable]
public class UI_Theme : ScriptableObject
{
    public TMP_FontAsset header1, header2, header3, paragraph;
    public Color primary, secondary, background, text;
}





