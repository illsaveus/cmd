﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class DataManager : MonoBehaviour
{

    public static DataManager instance = null;

    [SerializeField]
    SaveData saveData;
    [SerializeField]
    EventData currentEventData;

    public bool dontSave = false;


    static string path = "/IndiecadeInfo.dat";
    static string testingPath = "/IndiecadeInfoTesting.dat";

    private void Awake()
    {
        //Check if instance already exists
        if (instance == null)

            //if not, set instance to this
            instance = this;

        //If instance already exists and it's not this:
        else if (instance != this)

            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        DontDestroyOnLoad(gameObject);

        Reset();
    } 

    

    public void Reset()
    {
        Debug.Log("Persistent Data Path ---> " + Application.persistentDataPath + path);

        saveData = new SaveData();
        Load(); // load up all the data
    }



    public void Save()
    {
        
        Debug.Log(ToString() + "Saving Data");

        // open the file
        BinaryFormatter bf = new BinaryFormatter();

        FileStream file;

        if(dontSave)
            file = File.Create(Application.persistentDataPath + testingPath);
        else
            file = File.Create(Application.persistentDataPath + path);

        //save to file
        bf.Serialize(file, saveData); // serialize data to dataFile
        file.Close();
    }

    public void Load()
    {

        if (dontSave)
            return;


        if (File.Exists(Application.persistentDataPath + path))
        {
            Debug.Log(ToString() + "loading existing save file");
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + path, FileMode.Open);
            
            
            saveData = (SaveData)bf.Deserialize(file);            
            file.Close();

            Debug.Log("LOADED DATA: " + saveData.ToString());
        } else
        {
            Debug.Log(ToString() + "no save file found. Creating new.");
            // open the file
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Application.persistentDataPath + path);

            //write saveData info into file
            saveData = new SaveData();
            bf.Serialize(file, saveData); // serialize data to dataFile
            file.Close();
        }
    }

    public void ClearSavedData()
    {
        Debug.Log(ToString() + ": Clearing saved data");
        saveData = new SaveData();
        // open the file
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(Application.persistentDataPath + path);

        //write saveData info into file
        bf.Serialize(file, saveData); // serialize data to dataFile
        file.Close();
    }

    public void OpenSavedFile(string eventName)
    {
        currentEventData = saveData.GetSaveFileByName(eventName);
        if (currentEventData == null)
            Debug.Log(ToString() + " : Event '" + eventName + "' Not Found!");
        else
            Debug.Log(ToString() + ": Event Data Loaded -> " + currentEventData.m_name);
    }

    public string GetFirstEventName()
    {
        
        return currentEventData.GetEventDateByIndex(0).m_name;
    }

    public EventData GetEventData(string eventName)
    {
        return saveData.GetSaveFileByName(eventName);
    }

    public EventDate GetCurrentEventData(string currentEventDateName)
    {
        return currentEventData.GetEventDateByName(currentEventDateName); // load the current date selected
    }
 
    //
    public void CreateNewSave(string name, int dateNum)
    {
        Debug.Log(ToString() + ": Creating a new save | " + name + ", " + dateNum);

        saveData.CreateNewSave(name, dateNum);
        Save();

    }

    public List<string> GetListOfSaves()
    {
        return saveData.GetListOfEvents();        
    }

    public List<string> GetListOfDates()
    {
        return currentEventData.GetListOfDates();
    }

    public void ChangeEventName(string newName)
    {
        currentEventData.ChangeEventName(newName);
    }

    public string GetEventName()
    {
        return currentEventData.GetEventName();
    }
}

[System.Serializable]
public class SaveData
{
    public int numberOfSaves;
    public List<EventData> eventData = new List<EventData>();

    public SaveData()
    {
        numberOfSaves = 0;
    }
    
    public List<string> GetListOfEvents()
    {
        List<string> list = new List<string>();
        foreach (EventData e in eventData)
            list.Add(e.m_name);

        return list;
    }

    
    public void CreateNewSave(string name, int dateNumber)
    {
        numberOfSaves++;
        eventData.Add(new EventData(name, dateNumber));
    }

    // Return eventData that matched string parameter
    public EventData GetSaveFileByName(string name)
    {
        foreach (EventData e in eventData)
        {
            if (e.m_name == name)
                return e;
        }

        return null; //not found
    }

    public override string ToString()
    {
        return base.ToString() + " | Number of Saves: " + numberOfSaves;
    }
     
    


}

[System.Serializable]
public class EventData
{
    public string m_name;
    public int m_numberOfDates = 0;
    public List<EventDate> eventDates = new List<EventDate>();

    // must name the event before it's created
    public EventData(string name, int numberOfDates)
    {
        m_name = name;        

        for(int i = 0; i < numberOfDates; i++)
        {
            AddNewEvent("Day " + (i + 1));
        }
    }
        
    public void AddNewEvent(string name)
    {
        m_numberOfDates++;
        eventDates.Add(new EventDate(name));
        Debug.Log("Created new event date for " + m_name + " called " + name);
    }

    public EventDate GetEventDateByName(string name)
    {
        foreach (EventDate e in eventDates)
        {
            if (e.m_name == name)
                return e;
        }

        return null; //not found
    }

    public EventDate GetEventDateByIndex(int index)
    {
        return eventDates[index];
    }

    public List<string> GetListOfDates()
    {
        List<string> list = new List<string>();
        foreach (EventDate e in eventDates)
            list.Add(e.m_name);

        return list;

    }

    public string GetEventName()
    {
        return m_name;
    }

    public void ChangeEventName(string newName)
    {
        Debug.Log("Event  '" + m_name + "' is being changed to " + newName + " now.");
        m_name = newName;
    }
}

// contains all the transform data of all objects the player creates
[System.Serializable]
public class EventDate
{
    public string m_name;
    public List<string> prefabs = new List<string>();
    public List<SerializedVector3> positions = new List<SerializedVector3>();
    public List<SerializedVector3> rotations = new List<SerializedVector3>();
    public List<int> m_floorNum = new List<int>();
    public List<string> id = new List<string>();

    public List<SerializedVector3> cardPositions = new List<SerializedVector3>();
    public List<SerializedVector3> cardRotations = new List<SerializedVector3>();
    public List<int> m_cardFloorNum = new List<int>();
    public List<string> cardMessage = new List<string>();
    public List<string> card_id = new List<string>();

    // Must name the event Date before it's created
    public EventDate(string name)
    {
        m_name = name;
    }
    

    public int Length()
    {
        return prefabs.Count;
    }

    public int CardCount()
    {
        return cardMessage.Count;
    }

    public string GetPrefabName(int index)
    {
        return prefabs[index];
    }

    public string GetCardLabel(int index)
    {
        return cardMessage[index];
    }

    public Vector3 GetPosition(int index)
    {
        return positions[index].ToVector3();
    }

    public Vector3 GetCardPosition(int index)
    {
        return cardPositions[index].ToVector3();
    }

    public Quaternion GetRotation(int index)
    {
        return Quaternion.Euler(rotations[index].ToVector3());
    }

    public Quaternion GetCardRotation(int index)
    {
        return Quaternion.Euler(cardRotations[index].ToVector3());
    }

    public int GetFloorNum(int index)
    {
        return m_floorNum[index];
    }

    public int GetCardFloorNum(int index)
    {
        return m_cardFloorNum[index];
    }

    public void Add(string name, Vector3 pos, Vector3 rot, int floorNum)
    {
        prefabs.Add(name);
        positions.Add(new SerializedVector3(pos));
        rotations.Add(new SerializedVector3(rot));
        m_floorNum.Add(floorNum);
        id.Add(new SerializedVector3(pos).ToString() + new SerializedVector3(rot).ToString() + floorNum);

        //Debug.Log(ToString() + " : Creating --> " + new SerializedVector3(pos).ToString() + new SerializedVector3(rot).ToString() + " "  + floorNum);
    }

    public void AddCardLabel(string label, Vector3 pos, Vector3 rot, int floorNum)
    {
        
        cardPositions.Add(new SerializedVector3(pos));
        cardRotations.Add(new SerializedVector3(rot));
        m_cardFloorNum.Add(floorNum);
        cardMessage.Add(label);
        card_id.Add(new SerializedVector3(pos).ToString() + new SerializedVector3(rot).ToString() + floorNum);

        Debug.Log("Saving Card #" + CardCount() + ": " + label);
    }

    public void Remove(Vector3 pos, Vector3 rot, int floorNum)
    {
        int finder = id.FindIndex(x => x == new SerializedVector3(pos).ToString() + 
                    new SerializedVector3(rot).ToString() + floorNum);


        if(finder != -1)
        {
            Debug.Log("Removing Item ---> " + ToString() + " : " + id[finder]);
            prefabs.RemoveAt(finder);
            positions.RemoveAt(finder);
            rotations.RemoveAt(finder);
            m_floorNum.RemoveAt(finder);
            id.RemoveAt(finder);
        }
        else
        {
            finder = card_id.FindIndex(x => x == new SerializedVector3(pos).ToString() +
                    new SerializedVector3(rot).ToString() + floorNum);


            if (finder != -1)
            {
                Debug.Log("Removing Card ---> "+ ToString() + " : " + card_id[finder]);
                //prefabs.RemoveAt(finder);
                cardPositions.RemoveAt(finder);
                cardRotations.RemoveAt(finder);
                m_cardFloorNum.RemoveAt(finder);
                cardMessage.RemoveAt(finder);
                card_id.RemoveAt(finder);
            }
        }
    }

    public override string ToString()
    {
        string msg = "";
        int count = prefabs.Count;

        for (int i = 0; i < count; i++)
        {
            msg = msg + " [" + i + "] ";
            msg += prefabs[i];
            msg += positions[i].ToString();
            msg += " ";
            msg += rotations[i].ToString();
        }

        return base.ToString() + " - " + msg;
    }
}

[System.Serializable]
public class SerializedVector3
{
    public float m_x, m_y, m_z;

    public SerializedVector3()
    {
        m_x = m_y = m_z = 0.0f;
    }

    public SerializedVector3(Vector3 v)
    {
        m_x = v.x;
        m_y = v.y;
        m_z = v.z;
    }

    public Vector3 ToVector3()
    {
        return new Vector3(m_x, m_y, m_z);
    }

    public override string ToString()
    {
        string msg = "(" + m_x + ", " + m_y + ", " + m_z + ")";
        return msg;
    }
}