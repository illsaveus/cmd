﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "New Inventory Item", menuName = "Inventory Item")]
[System.Serializable]
public class InventoryItem : ScriptableObject
{
    
    public new string name;
    
    public string description;
    
    public string category;
    
    public string dimensions = "";
    
    public Sprite icon;
    
    public GameObject prefab;


    public void Print()
    {
        Debug.Log(name + ": " + description + " : " + category);
    }

}


[CreateAssetMenu(fileName = "New Inventory Label", menuName = "Inventory Label")]
public class InventoryLabel: InventoryItem
{
    public string label = "";
}
