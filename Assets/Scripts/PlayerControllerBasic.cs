﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerBasic : MonoBehaviour
{
    // Used to move the player relative to the direction of the camera
    public Transform camForward; // this is to help visualize and debug transition from Birds eye to FPS, while in build mode

    Camera cam;
    Rigidbody rb;
    bool GodMode = false;
    public float runSpeed = 10.0f;
    

    public bool isLocked = false;

    // This tracks the birds eye cam, but this can be locked
    // that way CM cam can told to "follow" this instead, effectively locking it from movement
    // CM Birds eye (unlocked), follows the player. Then, when locked CM brain switches to 
    // CM Birds eye (locked) follows this tracker instead. This tracker freezes in places in build mode
    public bool inBirdsEyeViewMode = false;

    // Use this for initialization
    void Start()
    {   

        rb = GetComponent<Rigidbody>();
        cam = GetComponentInChildren<Camera>();
        if (!cam)
            Debug.LogError("FPS Camera not found");

        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        GetInput(); // get general input from the player
    }

    void FixedUpdate()
    {
        Movement(); // movement input and logic        
    }

    float inputVertical, inputHorizontal;
    bool sprinting;
    Vector3 camRotation;

    // Get input from the player
    void GetInput()
    { 
        // move relative to the camera position when in birds eye view
        // track the forward direction of the camera using a tracker gameobject
        camRotation = new Vector3(0, cam.transform.rotation.eulerAngles.y, 0); 
        camForward.SetPositionAndRotation(transform.position, Quaternion.Euler(camRotation));
         

        inputVertical = Input.GetAxis("Vertical");
        inputHorizontal = Input.GetAxis("Horizontal");
        sprinting = Input.GetKey(KeyCode.LeftShift);
    }

    public void UpdateRunSpeed(float value)
    {
        runSpeed = value;
    }

    // Take input from the player to apply to movement
    void Movement()
    {
        if(isLocked)
            return;

            
        // Movement Input properties        
        float targetSpeed = (sprinting) ? runSpeed + runSpeed/2 : runSpeed;
        float translation = inputVertical * targetSpeed;
        float straffe = inputHorizontal * targetSpeed;
        translation *= Time.deltaTime;
        straffe *= Time.deltaTime;
        
        
        
        if(!inBirdsEyeViewMode)
        {
            // Rotate the character by the same amount the camera is rotated
            transform.rotation = Quaternion.Euler(new Vector3(0, cam.transform.rotation.eulerAngles.y, 0));

            // move the player forward 
            transform.Translate(straffe, 0, translation);
        } else { 
            
            transform.Translate(straffe, 0, translation, camForward); 
        } 
    }

    public void SnapToCamForward()
    { 
        Debug.Log("snapping");
        //transform.rotation = camForward.rotation;
    }

    
}

