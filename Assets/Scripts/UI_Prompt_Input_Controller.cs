﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using TBB;
public class UI_Prompt_Input_Controller : MonoBehaviour
{
    public DragAndDrop pc;
    

    public TMP_InputField inputfield;

    public void CloseInput()
    {
        pc.SetCardLabelTo(inputfield.text);

    }


    public void SelectMe()
    {
        inputfield.Select();
    }
}


/*
 
     //hide cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
     LockCamera(true);
    GameObject.Destroy(currentPreview);
     selectedObject = currentPreview = currentInventoryItem = null;
     SetRemoveToolTo(false);
     */
