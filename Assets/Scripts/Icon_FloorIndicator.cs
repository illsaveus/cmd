﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Icon_FloorIndicator : MonoBehaviour
{
    [SerializeField]
    Button [] buttons;
    public BuildingManager bm;


    private void Awake()
    {
        buttons = GetComponentsInChildren<Button>();
    }

    private void Start()
    {
        for(int i = 0; i < buttons.Length; i++)
        {
            int index = i;
            buttons[i].onClick.AddListener(
                delegate {
                    
                    ChangeFloorTo(index);
                });

            
        }
    }

    public void UpdateFloorIndicatorTo(int floorNum)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            if (i == floorNum)
                buttons[i].gameObject.GetComponent<Icon_FloorIndicator_Button>().Selected(true);
            else
                buttons[i].gameObject.GetComponent<Icon_FloorIndicator_Button>().Selected(false);
        }
        
    }

    public void ChangeFloorTo(int floorNum)
    {
        bm.MoveToFloor(floorNum);
    }

}
