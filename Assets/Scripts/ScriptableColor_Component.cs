﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScriptableColor_Component : MonoBehaviour
{ 
    public ScriptableColor color;
    public bool refresh = false;
    // Start is called before the first frame update
    void Start()
    {
        LoadData();
    }

    // Update is called once per frame
    void OnValidate()
    {
        LoadData();
    }

    void LoadData()
    {
        refresh = false;
        if (!color)
            return;

        if (GetComponent<Image>())
        {
            GetComponent<Image>().color = color.value;
        }
        //else Debug.Log("Image asset not found");

        if (GetComponent<TextMeshProUGUI>())
        {
            GetComponent<TextMeshProUGUI>().color = color.value;
        }
        //else Debug.Log("TMP Font asset not found");
    }
}
