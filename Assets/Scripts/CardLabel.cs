﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CardLabel : MonoBehaviour
{ 
    [SerializeField]
    TextMeshPro tmp;

    string text; 

    public void UpdateLabel(string msg)
    {
        text = msg;

        if (tmp)
            tmp.text = text;
    }
}
