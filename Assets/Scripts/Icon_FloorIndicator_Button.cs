﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Icon_FloorIndicator_Button : MonoBehaviour
{
    public GameObject[] icons;

    public void Selected(bool value)
    {
        icons[0].gameObject.SetActive(value);
        icons[1].gameObject.SetActive(!value);
    }

}
