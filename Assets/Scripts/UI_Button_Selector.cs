﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Button_Selector : MonoBehaviour
{
     
        public GameObject overlay;

        public void Selected(bool value)
        {
            overlay.gameObject.SetActive(value);            
        }

}
