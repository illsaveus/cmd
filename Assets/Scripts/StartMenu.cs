﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

namespace TBB
{
    public class StartMenu : MonoBehaviour
{
    public bool inDebugMode = false;
    public GameObject clearSaveDataButton;

    DataManager data;

    [SerializeField]
    GameObject win_mainMenu, win_newSave;

    [SerializeField]
    Image bg;

    public Dropdown eventPicker;
    public TMP_InputField newEventName, newEventDayNum;

    string currentEventDateName = "";

    // Start is called before the first frame update
    void Start()
    {

        ActivateDebugGUI();
        

        data = GameObject.FindGameObjectWithTag("Data Manager").GetComponent<DataManager>();
        if (!data)
            Debug.LogError("Data Manager Not Found!");

        ResizeBG();

        // Open the saved file and populate the dropdown list of saves
        LoadEventDropdownMenu(); 

        //Add listener for when the value of the Date Picker Dropdown changes, to take action
        eventPicker.onValueChanged.AddListener(delegate {
            ChangeCurrentEventDate(eventPicker);
        });

        
    }

    void ActivateDebugGUI()
    {
        
        clearSaveDataButton.SetActive(inDebugMode);

    }

    // for debugging, this will delete all saved data and create a brand new default data file
    public void ClearSavedData()
    {
        data.ClearSavedData();
        LoadEventDropdownMenu();
    }

    // Query Data for a list of all the Saved Events in data file
    void LoadEventDropdownMenu()
    {
        // clear drop down menu of current list of options
        eventPicker.ClearOptions();

        List<string> list = data.GetListOfSaves();

        // add "new" option to the end
        list.Add("Create New");
        
        eventPicker.AddOptions(list);

        // Make sure the currentEventDate in data is set to the first option by default
        ChangeCurrentEventDate(eventPicker);
    }

     


    void ResizeBG()
    {
        //get size of canvas
        float width = Screen.width;
        float height = Screen.height;

        if(width > height)        
            bg.rectTransform.sizeDelta = new Vector2(width, width*4);        
        else
            bg.rectTransform.sizeDelta = new Vector2(height*4, height);
    } 

    public void ChangeCurrentEventDate(Dropdown change)
    {
        currentEventDateName = eventPicker.options[change.value].text;
        Debug.Log("changing date to " + currentEventDateName);

        if (currentEventDateName == "Create New")
            return;
        else
            data.OpenSavedFile(currentEventDateName); // load the current date selected
    }

    public void LoadCMDScene()
    {
        if (currentEventDateName == "Create New")
            OpenNewSaveWindow();
        else 
            // must have an eventData set up first!        
            StartCoroutine(LoadYourAsyncScene());
    }

    public void CreateNewSave()
    {
        //Create a new event using the name from ui input
        int dateNum;
        int.TryParse(newEventDayNum.text, out dateNum);

        if (dateNum <= 0)
            dateNum = 3; // set default number of dates to 3

        data.CreateNewSave(newEventName.text, dateNum);

        //Update current event name to this so Load finds it
        currentEventDateName = newEventName.text;
        data.OpenSavedFile(currentEventDateName); // load the current date selected

        LoadCMDScene();
    }

    void OpenNewSaveWindow()
    {
        win_mainMenu.SetActive(false);
        win_newSave.SetActive(true);
    }

    public void BackToMainMenu()
    {
        win_newSave.SetActive(false);
        win_mainMenu.SetActive(true);
        
    }

    IEnumerator LoadYourAsyncScene()
    {
        Debug.Log("LOADING CMD SCENE");
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(1);

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }

    public void ExitApplication()
    {
        Application.Quit();
    }
}

}
