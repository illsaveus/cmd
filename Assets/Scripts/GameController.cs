﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    
    public DataManager data;
    EventDate currentEventDate;
    public InventoryList inventoryList;
    public Dropdown datePicker;
    string currentEventDateName;
    public BuildingManager buildingManager;
       
    private void Start()
    {
        var finder = GameObject.FindGameObjectWithTag("Data Manager");
        if (finder)
        {
            data = GameObject.FindGameObjectWithTag("Data Manager").GetComponent<DataManager>();
            LoadEventData();
        }

        LoadDatePickerDropdownMenu();


        //Add listener for when the value of the Date Picker Dropdown changes, to take action
        datePicker.onValueChanged.AddListener(delegate {
            ChangeCurrentEventDate(datePicker);
        });
    }

    public void LoadHomeMenuScene()
    {
        
        StartCoroutine(LoadYourAsyncScene());
    }



    IEnumerator LoadYourAsyncScene()
    {
        Debug.Log("LOADING START Menu SCENE");
        // The Application loads the Scene in the background as the current Scene runs.
        // This is particularly good for creating loading screens.
        // You could also load the Scene by using sceneBuildIndex. In this case Scene2 has
        // a sceneBuildIndex of 1 as shown in Build Settings.

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Start Menu");

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone)
        {
            data.Reset();
            yield return null;
        }
    }

    void LoadDatePickerDropdownMenu()
    {
        // clear drop down menu of current list of options
        datePicker.ClearOptions();

        if(data)
        {
            List<string> list = data.GetListOfDates();
            
            datePicker.AddOptions(list);

            // Make sure the currentEventDate in data is set to the first option by default
            ChangeCurrentEventDate(datePicker);
        }   
    }

    public void Save()
    {
        if(data)
        {
            data.Save();
            // TODO: GUI popup confirmation "Saved"
            UI_panel_notification.SetActive(true);
            StartCoroutine("NotificationTimer");
        }

        
    }

    public void LoadEventData()
    {
        if (!data)
            return;


        currentEventDateName = data.GetFirstEventName();
        currentEventDate = data.GetCurrentEventData(currentEventDateName);        

        ResetSceneOnLoad();
    }

    public void RenameCurrentEvent(string newName)
    {
        data.ChangeEventName(newName);
        Save();
    }

    public string GetEventName()
    {
        return data.GetEventName();
    }


    public void CreateNewInventoryObject(InventoryItem prefab, Vector3 position, Quaternion rotation, int floorNum)
    {   
        currentEventDate.Add(prefab.name, position, rotation.eulerAngles, floorNum);
    }

    public void CreateNewLabel(CardLabelController card, Vector3 position, Quaternion rotation, int floorNum)
    {
        currentEventDate.AddCardLabel(card.cardLabel,  position,  rotation.eulerAngles,  floorNum);
    }

    public void DeleteInventoryObject(GameObject item, int floorNum)
    {
        buildingManager.RemoveItemFromFloor(item);

        currentEventDate.Remove(item.transform.position, item.transform.rotation.eulerAngles, floorNum);
    }

    

     

    void ResetSceneOnLoad()
    {
        ClearScene();

        if (!data)
            return;

        //Debug.Log("Adding items from data file...");
        // search through all the regular inventory prefabs in eventData (saved data for specific event)
        for(int i = 0; i < currentEventDate.Length(); i++)
        {
            InventoryItem item = inventoryList.FindItemByName(currentEventDate.GetPrefabName(i));
            GameObject newItem = Instantiate(item.prefab, currentEventDate.GetPosition(i), currentEventDate.GetRotation(i));

            buildingManager.AddItemToFloor(newItem, currentEventDate.GetFloorNum(i)); // this will parent the item to a specific floor

        }

        // search through all the card labels in eventData (saved data for specific event)
        for (int i = 0; i < currentEventDate.CardCount(); i++)
        {
            InventoryItem item = inventoryList.FindItemByName("Card Label");
            GameObject newItem = Instantiate(item.prefab, currentEventDate.GetCardPosition(i), currentEventDate.GetCardRotation(i));
            newItem.GetComponent<CardLabelController>().cardLabel = currentEventDate.GetCardLabel(i);

            buildingManager.AddItemToFloor(newItem, currentEventDate.GetCardFloorNum(i)); // this will parent the item to a specific floor

        }

        buildingManager.UpdateFloorDisplayedTo(buildingManager.GetCurrentFloor());
        //Debug.Log("DONE adding data");

    }

    public void ClearScene()
    { 
        //Debug.Log("Removing inventory items");
        GameObject [] items = GameObject.FindGameObjectsWithTag("Inventory Item");

        foreach (GameObject i in items)
            Destroy(i);

        buildingManager.ClearFloorItems();
    }

    
    public void ChangeCurrentEventDate(Dropdown change)
    {
        if (!data)
            return;

        string name = datePicker.options[change.value].text;

        currentEventDateName = name;
        //Debug.Log("changing date to " + currentEventDateName);

        currentEventDate = data.GetCurrentEventData(currentEventDateName); // load the current date selected
        ResetSceneOnLoad();
    }


    // TODO
    public void ExitToMainMenu()
    {

    }

    // TODO
    public void OpenOptionsMenu()
    {

    }

    public GameObject UI_panel_notification;
    public float UI_notificationTimer = 0.7f;
    IEnumerator NotificationTimer()
    {
        for (float i = UI_notificationTimer; i >= 0; i -= 0.1f)
        {
            yield return new WaitForSeconds(.1f);
        }
        UI_panel_notification.SetActive(false);
    }

    

    
}



