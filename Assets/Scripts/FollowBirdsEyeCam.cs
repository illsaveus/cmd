﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowBirdsEyeCam : MonoBehaviour
{
    public Camera cam;
    public GameObject character;
    public bool isLocked = false;

    // Update is called once per frame
    void Update()
    {
        if(isLocked)
            return;
        else
        {
            transform.rotation = cam.transform.rotation;
            transform.position = cam.transform.position;
        }
    }
}

