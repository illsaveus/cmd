﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewMode : MonoBehaviour
{
    int layerNumber = 10;
    public bool isBuildable;
    public List<Collider> collisions = new List<Collider>();
    

    private void Start()
    {
//        Debug.Log("enabled triggers");

        Collider[] col = GetComponentsInChildren<Collider>();
        foreach (Collider c in col)
        {
            c.isTrigger = true;
        }
    }
     
    

    void Update()
    {
        CheckCollision();
    }

    public bool IsBuildable()
    {
        return isBuildable;
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == layerNumber)
            collisions.Add(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == layerNumber)
            collisions.Remove(other);
    }

    public void CheckCollision()
    {        
        if (collisions.Count == 0)
            isBuildable = true;        
        else        
            isBuildable = false;        
    }
}
