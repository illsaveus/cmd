﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveMode : MonoBehaviour
{

    // Preview Materials/Shaders
    Renderer previewRenderer;  // contain the materials for the object
    public Material  matRed;
    public List<Collider> collisions = new List<Collider>();
    public List<Material> materials = new List<Material>();
    bool isSelected = false;

    private void Start()
    {
        Collider[] col = GetComponentsInChildren<Collider>();
        foreach (Collider c in col)
        {
            c.isTrigger = true;
        }

        Renderer [] mats = GetComponentsInChildren<Renderer>();
        foreach(Renderer m in mats)
        {
            materials.Add(m.material);
        }   
    }
    public bool IsSelected()
    {
        return isSelected;
    }
    void Update()
    {
        CheckCollision();
    }

    public void TurnRed()
    {
        var mat = GetComponent<Renderer>();
        if (mat)
            mat.material = matRed;

        foreach (Transform child in transform)
        {
            if(child.GetComponent<Renderer>())
                child.GetComponent<Renderer>().material = matRed;
        }
    }

    public void RemoveRed()
    {
        Renderer[] mats = GetComponentsInChildren<Renderer>();
        for(int i = 0; i < mats.Length; i++)
        {
            mats[i].material = materials[i];
        }
         
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Remove Tool"))
            collisions.Add(other);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Remove Tool"))
            collisions.Remove(other);
    }

    public void CheckCollision()
    {
        if (collisions.Count == 0)
            isSelected = false;
        else
            isSelected = true;


        if (isSelected)
            TurnRed();
        else
            RemoveRed();
    }
}
