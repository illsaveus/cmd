﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MenuManager : MonoBehaviour
{
    public GameObject[] menuLists;

     public void ChangeMenuTo(GameObject newMenu)
    {
        foreach(GameObject m in menuLists)
        {
            if (m == newMenu)
                m.SetActive(true);
            else
                m.SetActive(false);

        }
    }
}
