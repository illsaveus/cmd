﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Scriptable Color - ", menuName = "UI Themes/Scriptable Color")]
[System.Serializable]
public class ScriptableColor : ScriptableObject
{
    public Color value;
}


