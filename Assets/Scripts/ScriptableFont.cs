﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "Scriptable Font - ", menuName = "UI Themes/Scriptable Font")]
[System.Serializable]
public class ScriptableFont : ScriptableObject
{
    public TMP_FontAsset asset;
}
