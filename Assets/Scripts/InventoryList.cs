﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Inventory List", menuName = "Inventory List")]
[System.Serializable]
public class InventoryList : ScriptableObject
{
    public InventoryItem [] items;


    public InventoryItem FindItemByName(string name)
    {
        foreach(InventoryItem i in items)
        {
            if (i.name == name)
                return i;
        }

        return null; // not found
    }
}
