﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class RemoveTool : MonoBehaviour
{

    public List<Transform> collisions = new List<Transform>();

 

    // look for which object is selected to be removed and return a reference to that so that DragAndDrop can remove it from data as well as the scene
    public GameObject DeleteSelected()
    {
        foreach(Transform c in collisions)
        {
            if(c.gameObject.GetComponent<RemoveMode>().IsSelected())
            {
                collisions.Remove(c);
                return c.gameObject;
            }
        }

        return null; //no selected object found
    }


    private void OnTriggerEnter(Collider other)
    {
        

        if (other.gameObject.layer == 10)
        {
             

            if (other.gameObject.GetComponentInParent<RemoveMode>())
                if(other.gameObject.transform.parent)
                {
                    if(!collisions.Contains(other.gameObject.transform.parent))
                        collisions.Add(other.gameObject.transform.parent);

                }
        }
            
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == 10)
        { 

            if (other.gameObject.GetComponentInParent<RemoveMode>())
                collisions.Remove(other.gameObject.transform.parent);
        }
    }

    
}
