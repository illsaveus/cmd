﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class UI_Theme_Component : MonoBehaviour
{
    public UI_Theme theme;
    public enum TextClass{
        header1, header2, header3, paragraph
    }

    public TextClass textClass;

    public bool refresh = false;

    private void OnValidate()
    {
        LoadThemeStyle();
        refresh = false;
    }

    private void Awake()
    {
        LoadThemeStyle();
    }

    void LoadThemeStyle()
    {
        if (!theme)
            return;

        if(GetComponent<Image>())
        {
            GetComponent<Image>().color = theme.background;
        }

        if (GetComponent<TextMeshProUGUI>())
        {

            GetComponent<TextMeshProUGUI>().color = theme.text;
            GetComponent<TextMeshProUGUI>().font = GetFontByClass();
        }
    }

    TMP_FontAsset GetFontByClass()
    {
        switch(textClass)
        {
            case TextClass.header1:
                return theme.header1;
                

            case TextClass.header2:
                return theme.header2;
                

            case TextClass.header3:
                return theme.header3;
                

            case TextClass.paragraph:
                return theme.paragraph;
                

            default: return null;
        }
    }
 
}
