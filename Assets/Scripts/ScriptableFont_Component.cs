﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScriptableFont_Component : MonoBehaviour
{
    
    public ScriptableFont font;
    public bool refresh = false;
    // Start is called before the first frame update
    void Start()
    {
        LoadData();
    }

    // Update is called once per frame
    void OnValidate()
    {
        LoadData();
    }

    void LoadData()
    {
        refresh = false;
        if (!font)
            return;

        if (GetComponent<TextMeshProUGUI>())
        {
            GetComponent<TextMeshProUGUI>().font = font.asset;
        }
        //else            Debug.Log("TMP Font asset not found");
    }
}