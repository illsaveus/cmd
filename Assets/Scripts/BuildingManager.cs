﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TBB;

public class BuildingManager : MonoBehaviour
{
    // TODO: determine what the current floor index is, based on what floor the player is in, currently
    // make sure to handle transitions where the player is technically on both floors, like on the stairs
    // use a box trigger

    public Camera cam;
    public DragAndDrop player;
    public GameObject [] floors;
    public Icon_FloorIndicator floorIndicatorIcon;
    int floorIndex = 0;

    public List<GameObject> floor1 = new List<GameObject>();
    public List<GameObject> floor2 = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        
    }

    /*
    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonDown("Level Up"))
        {
            floorIndex++;
            UpdateFloorDisplayed();
        }
        if(Input.GetButtonDown("Level Down"))
        {
            floorIndex--;
            UpdateFloorDisplayed();
        }

        // make sure the floorIndex is within the floor arrays indices
        floorIndex = Mathf.Clamp(floorIndex, 0, floors.Length-1);
        UpdateFloorDisplayed();
        
            
    }
    
     */

   public void UpdateFloorIndicator()
    {
        floorIndicatorIcon.UpdateFloorIndicatorTo(floorIndex);
    }

    public int GetCurrentFloor()
    {
        return floorIndex;
    }

    public bool UpdateFloorDisplayed()
    {
        return UpdateFloorDisplayedTo(floorIndex);
    }
     

    public bool MoveFloorUp()
    {
        return UpdateFloorDisplayedTo(floorIndex+1);
    }

    public bool MoveFloorDown()
    {
        return UpdateFloorDisplayedTo(floorIndex-1);
    }

    public void MoveToFloor(int floor)
    {
        
        UpdateFloorDisplayedTo(floor);
        player.MoveCharacterUpOneFloor();
    }

    // Update which floors are hidden v revealed, move character to that level
    public bool UpdateFloorDisplayedTo(int floorNum)
    {
        // TODO: change what is active on flower beneath the current one such that only the exterior walls are active
        // and all lighting and objects inside are hidden

        // make sure the floorIndex stays within the floor array's indices
        if(floorNum < 0 || floorNum >= floors.Length)
        {
            Debug.Log("You've reached the end.");
            return false; // respond with floor movement failed
        }
        //floorIndex = Mathf.Clamp(floorIndex, 0, floors.Length-1);

        floorIndex = floorNum;
        

        // hide floors above current one, reveal floors below
        for(int i = 0; i < floors.Length; i++)
        {
            if(i <= floorIndex)
                floors[i].SetActive(true);
            else
                floors[i].SetActive(false);
        }

        // hide/reveal the list of inventory items placed on each floor
        if (floorIndex == 0)
        {
            foreach(GameObject g in floor1)
                g.SetActive(true);

            foreach (GameObject g in floor2)
                g.SetActive(false);
        }
        else           
        {
            foreach (GameObject g in floor1)
                g.SetActive(false);

            foreach (GameObject g in floor2)
                g.SetActive(true);
        }

        UpdateFloorIndicator();

        // hide layers
        if (floorIndex == 0)
            cam.cullingMask = ~(1 << 14);
        else
            cam.cullingMask = ~(1 << 15);

        return true; // respond with floor transition successful
    }

    public void AddItemToFloor(GameObject item)
    {
        if (floorIndex == 0)
            floor1.Add(item);
        else
            floor2.Add(item);


        //item.transform.parent = floors[floorIndex].transform;
    }

    public void AddItemToFloor(GameObject item, int floorNum)
    {
        if (floorNum == 0)
            floor1.Add(item);
        else
            floor2.Add(item);

        floors[floorNum].SetActive(true);
        //item.transform.parent = floors[floorNum].transform; 
    }

    public void RemoveItemFromFloor(GameObject item)
    {
        if (floor1.Contains(item))
            floor1.Remove(item);
        else if (floor2.Contains(item))
            floor2.Remove(item);

    }

    public void ClearFloorItems()
    {
        //Debug.Log("clearing flor 1 and 2");
        floor1.Clear();
        floor2.Clear();
        floor1 = new List<GameObject>();
        floor2 = new List<GameObject>();
    }


    
}
