﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using TBB;

public class CardLabelController : MonoBehaviour
{
    public DragAndDrop pc;
    public string cardLabel = "Message Here";

    [SerializeField]
    TextMeshPro side1, side2;


    // Start is called before the first frame update
    void Start()
    {
        UpdateCardLabelTo(cardLabel);

    }

    public void UpdateCardLabelTo(string msg)
    {
        cardLabel = msg;
        side1.text = cardLabel;
        side2.text = cardLabel;
    } 

}
