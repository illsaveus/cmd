﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InventoryButton : MonoBehaviour
{
    

    public Image icon;
    public TextMeshProUGUI label;
    public TextMeshProUGUI dimensions;


    public InventoryItem inventoryItem;
    private ShopScrollList scrollList; // parent scroll list

    private void OnValidate()
    {
        Setup();
    }

    // Start is called before the first frame update
    void Start()
    {
        Setup();        
    }

    public void Setup( )
    {
        if (inventoryItem)
        {
            dimensions.text = inventoryItem.dimensions;
            if (inventoryItem.dimensions == "")
                dimensions.gameObject.SetActive(false);
            else
                dimensions.gameObject.SetActive(true);


            icon.sprite = inventoryItem.icon;
            label.text = inventoryItem.name;
        }
        else
        {
            Debug.LogError("Inventory item " + this.name + " does not have required inventory item object reference.");
        }
    }

    // setup the button with a given Inventory Item and a parent ShopScrollList
    public void Setup(InventoryItem item, ShopScrollList shopScrollList)
    {
        inventoryItem = item;
        scrollList = shopScrollList;
        Setup(); // change label and sprite, as you do
        

    }

    public void ChangePreviewItem()
    {
        GetComponentInParent<GUI_LowerThirds>().UpdatePreviewItem(this.inventoryItem);
    }

}
