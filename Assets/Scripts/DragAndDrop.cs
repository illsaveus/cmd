﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using TMPro;

namespace TBB
{
    public class InventoryObject
    {
        GameObject prefab;
    }

}

/*
*   BUILDING SEQUENCE:
*   1. OPEN INVENTORY WINDOW
*      A. Select an object from window
*      B. Building mode enabled
*   2. OBJECT PREVIEW APPEARS IN-WORLD, WHERE CAM IS LOOKING
*       A. Object preview moves with camera 
*       B. Preview does a collision check to make sure object can fit in space
*   3. PLACE OBJECT WITH KEY/BUTTON
*   4. CLOSE INVENTORY WINDOW
*       A. Building mode disabled
*
*/


/* TODO:
 * [X] Toggle building mode with key
 * [X] Instantiate object for placement
 * [X] Place object with key * 
 * [X] Colored outline for preview object (green = OK, red = cannot place there)
 * [X] Logic for checking placement is correct
 * [X] logic for rotating the object
 * [ ] logic for snapping the object
 * [X] inventory window 
 *      [X] populate it with objects
 * [ ] change of camera states works properly when not in building mode
 */

namespace TBB
{
    public class DragAndDrop : MonoBehaviour
{
    public GameController gm;
    PlayerControllerBasic character;
    public GameObject targetIndicator;
    public BuildingManager buildingManager;
    GameObject selectedObject;
    GameObject currentPreview;
    public GUI_LowerThirds gui_buildMode;
    public TextMeshProUGUI gui_cameraMode;
    public GameObject pauseMenu, removeTool;
    GameObject removeToolInstance;

    Vector3 previewPosition;
    Transform previewTransform;

    bool isPaused = false;

    public Camera cam;
    public RaycastHit hit;
    float raycastDistance = 100.0f;
    public LayerMask layerMask, inventoryLayer, tableSurface;
    


    bool buildingModeIsOn = false;
    bool buildingMenuIsOpen = false;
 
    bool rotateMode = false;
    Quaternion previewRotation = Quaternion.identity;
    float rotateAmount = 0;

    // Preview Materials/Shaders
    Renderer previewRenderer;  // contain the materials for the object
    public Material matGreen, matRed;

    // Animation State Machine *used by CM
    Animator anim;
    bool inBirdsEyeViewMode = false;

    public FollowCamera fpsCam;
    public FollowBirdsEyeCam birdsEyeCam;

    // debugging transition between birds and fps
    public Vector3 fpsOffset = Vector3.zero;
    public Transform previewTracker; // tracks fps transform while in birds eye mode

    public CinemachineVirtualCamera cm_fpsCamUnlocked;
    CinemachinePOV cm_pov;
    public CinemachineFreeLook cm_BirdsEyeCamUnlocked;
    public AxisState axis_h, axis_v;
    bool inCamTransition = false;
    bool removeToolIsActive = false;

    
    bool placingTableCard = false;

    // Start is called before the first frame update
    void Start()
    {

        
        targetIndicator.SetActive(false);
        //cam = GetComponentInChildren<Camera>();
        character = GetComponent<PlayerControllerBasic>();
        
        anim = GetComponent<Animator>();

        if(!anim)
            Debug.LogError("Animator not found");

        // Collect CM cams
        cm_pov = cm_fpsCamUnlocked.GetCinemachineComponent<CinemachinePOV>();
        if (!cm_pov)
            Debug.LogError("CM POV cam missing!");
        if(!cm_BirdsEyeCamUnlocked)
            Debug.LogError("CM Freecam missing!");


        if(inBirdsEyeViewMode)
            gui_cameraMode.text = "Birds Eye View"; 
        else
            gui_cameraMode.text = "First Person View";


        GetPlayerPrefs();
        buildingManager.UpdateFloorIndicator();

        SetCursorLockedTo(true);
        LockCamera(false);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateCurrentPreviewTracker();

        // When a cam transition takes places, check when it finishes
        if(inCamTransition)
            CheckWhen_CM_BlendIsFinished();

        if (inBirdsEyeViewMode)
            UpdateFPSTracker();


        // GUI is paused for input for the cardlabel
        if (!inputModeIsOn)
            GetInput();
        else if (Input.GetButton("Submit"))
            UI_prompt_input.GetComponent<UI_Prompt_Input_Controller>().CloseInput();


        if (isPaused)
            return;


        if (buildingModeIsOn)
            BuildingMode();
        else if (inBirdsEyeViewMode)
            MatchTargetIndicatorRotationToCam();

        
        //Debug.Log(cm_pov.m_HorizontalAxis.Value + " : " + cm_pov.m_VerticalAxis.Value);
    }

    public void SetRemoveToolTo(bool value)
    {
        removeToolIsActive = value;

        if (removeToolIsActive)
        {
            selectedObject = removeTool;
            GameObject.Destroy(currentPreview);
            InitBuildingMode();
            buildingMenuIsOpen = false;
            PreviewObject();
        }
        else
            selectedObject = null;
    }

    // Indicator image will rotate along with the cam
    void MatchTargetIndicatorRotationToCam()
    {
        float y = cam.transform.rotation.eulerAngles.y;
        Vector3 indicatorXYZ = targetIndicator.transform.rotation.eulerAngles;
        indicatorXYZ.y = y;

        targetIndicator.transform.rotation = Quaternion.Euler(indicatorXYZ);
    }

    void SetPOVCamForwardTo(Quaternion target)
    {      
        Vector3 rot = target.eulerAngles;

        // grab values from cam to placeholder
        axis_h = cm_pov.m_HorizontalAxis;
        axis_v = cm_pov.m_VerticalAxis;

        // set placeholder values
        axis_h.Value = rot.y;
        axis_v.Value = rot.x;
        // apply placeholder values to cam
        cm_pov.m_HorizontalAxis = axis_h;
        cm_pov.m_VerticalAxis = axis_v;

        //Debug.Log("Setting POV ... h:" + rot.y + " v:" + rot.x);
    }
    
    // this is to help visualize and debug transition from Birds eye to FPS, while in build mode
    void UpdateFPSTracker()
    {
        // moving to character position
        previewTracker.position = character.transform.position;

        // then apply offset to position
        previewTracker.Translate(fpsOffset);

        // grab rotation, but only the Y rotation
        Quaternion rotation = Quaternion.LookRotation(fpsCam.transform.forward, Vector3.up);
        Vector3 newRot = rotation.eulerAngles;

        // apply rotation
        previewTracker.rotation = Quaternion.Euler(new Vector3(0, newRot.y, 0));
    }

    // This checks when the visual transition between camera states is complete
    void CheckWhen_CM_BlendIsFinished()
    {
        // FPS to Birds
        if(inBirdsEyeViewMode)
        {
            if(!CinemachineCore.Instance.IsLive(cm_fpsCamUnlocked))
            {
                Debug.Log("FPS -> Birds: blend is finished");
                inCamTransition = false;
                character.isLocked = false;
                fpsCam.isLocked = false;

                if (buildingMenuIsOpen)
                {
                    anim.SetBool("isLocked", true);
                    character.isLocked = true;
                }
            }
            else
            {
                character.isLocked = true;
                fpsCam.isLocked = true;
            }
        }
        // Birds to FPS
        else
        {
            if (!CinemachineCore.Instance.IsLive(cm_BirdsEyeCamUnlocked))
            {
                Debug.Log("Birds -> FPS: blend is finished");
                inCamTransition = false;
                character.isLocked = false;
                fpsCam.isLocked = false;
                //cm_BirdsEyeCamUnlocked.LookAt = this.gameObject.transform;

                if(buildingMenuIsOpen)
                {
                    anim.SetBool("isLocked", true);
                    character.isLocked = true;
                }

            }
            else
            {
                // change the look at target????
                //cm_BirdsEyeCamUnlocked.LookAt = null;
                character.isLocked = true;
                fpsCam.isLocked = true;
            }
        }
    }

    // Changes the camera between FPS and Birds eye view states
    public void ChangeCameraStates()
    {
        

        anim.SetBool("isLocked", false);

        inBirdsEyeViewMode = !inBirdsEyeViewMode;
        inCamTransition = true;
        // right now it moves character to center of the map (i think)
        if(inBirdsEyeViewMode)
        {
            gui_cameraMode.text = "Birds Eye View";
            fpsCam.isActive = true;

            anim.SetTrigger("Change to Birds Eye View");
            
            
            character.inBirdsEyeViewMode = true;

            // reset Birds Eye Cam (CM Freelook) so that it's behind the player orientation
            cm_BirdsEyeCamUnlocked.m_XAxis.Value = 0;

            // if in building mode, move to the position of the preview, otherwise, turn on location indicator
            if (buildingModeIsOn)
                transform.position = previewPosition; 
            else 
                targetIndicator.SetActive(true);


            if (currentPreview)
                targetIndicator.SetActive(false);
        }
        else
        {
            gui_cameraMode.text = "First Person View";
            fpsCam.isActive = true;
            
            anim.SetTrigger("Change to FPS View");
            

            character.inBirdsEyeViewMode = false;

            // move the character position at an offset from the preview position
            // must be relative to the orientation of the birds eye camera. Must be a point between them. (previewTracker has this position)
            // i think i can just use the fps transform tracker object 
            //Debug.Log("moving to preview tracker pos and rot");

            // if building is on, there will be a preview object, in which case you want to transition into FPS into an offset position (previewTracker object)
            if (buildingModeIsOn)
            {
                // POV camera needs to be updated on the orientation of the player but looking at exactly the same spot
                //previewTracker.transform.LookAt(currentPreview.transform);

                // move player position and rotation to the preview tracker
                transform.position = previewTracker.position;
                transform.rotation = previewTracker.rotation;

                UpdateCurrentPreviewTracker();

                // look at the currentPreview object                
                SetPOVCamForwardTo(previewTracker.rotation);                
                fpsCam.gameObject.transform.rotation = lookAtCurrentPreviewTracker.transform.rotation;
                SetPOVCamForwardTo(lookAtCurrentPreviewTracker.transform.rotation);
            } // with building mode OFF there is no preview object, so you don't need to move into the previewTracker offset position
            else
            {
                targetIndicator.SetActive(false);
                // POV camera needs to be updated on the orientation of the player
                SetPOVCamForwardTo(previewTracker.rotation);
                // set fpsCAM tracker object to the player position and rotation
                fpsCam.gameObject.transform.rotation = transform.rotation;
            }
        }

    }

    // 
    public Transform lookAtCurrentPreviewTracker;
    void UpdateCurrentPreviewTracker()
    {   
        if (currentPreview)
        {
            lookAtCurrentPreviewTracker.transform.position = previewTracker.position + new Vector3(0, 1.59f,0) ;
            lookAtCurrentPreviewTracker.transform.LookAt(currentPreview.transform.position);
            //Debug.Log("tracking");
        }   
    }

    public void ToggleOptionsMenu()
    {
        isPaused = !isPaused;
        PauseMenuSetActiveTo(isPaused);
    }

    public void PauseMenuSetActiveTo(bool value)
    {
        if(value)
        {
            //reveal cursor
            SetCursorLockedTo(false);
            LockCamera(true);

            pauseMenu.SetActive(true);
            pauseMenu.GetComponent<PauseMenu>().ActivatePauseMenu();
        }
        else
        {
            pauseMenu.SetActive(false);
            //reveal cursor
            SetCursorLockedTo(true);
            LockCamera(false);
        }

        isPaused = value;
    }

   

    // Handles general input from the user
    void GetInput()
    {
        

        // Camera mode toggle switching
        if (Input.GetButtonDown("Toggle Camera State"))
        {
            Debug.Log("changing camera states");

            ChangeCameraStates();
        }



        if (Input.GetButtonDown("Escape"))
            ExitBuildingMode();
            



        



        if (!isPaused)
        {
            // collect input from player that allows for object rotation
            rotateMode = Input.GetButton("Rotate Mode");
        
            // Build Mode toggle switching
            if(Input.GetButtonDown("Build Mode"))
            { 
                buildingMenuIsOpen = !buildingMenuIsOpen;
                //Debug.Log("Building Mode : " + buildingModeIsOn);

                if (gui_buildMode)
                {
                    if(buildingMenuIsOpen)
                    {
                        //open build menu
                        gui_buildMode.Activate();
                        buildingModeIsOn = false; 

                        //reveal cursor
                        SetCursorLockedTo(false);
                        LockCamera(true);
                    } else 
                    { 
                        //close build menu
                        gui_buildMode.Deactivate();

                        // check whether there's a preview object selected or not (if so, stay in building mode)
                        if (currentPreview)
                            buildingModeIsOn = true; 
                        else
                            buildingModeIsOn = false;

                        //hide cursor
                        SetCursorLockedTo(true);
                        LockCamera(false);
                    }
                } 
            }

            // moving the character between floors requires telling the method how far to move vertically
            if(Input.GetButtonDown("Level Up"))
            {
                if (buildingManager.MoveFloorUp())
                    MoveCharacterUpOneFloor();
            }
            if(Input.GetButtonDown("Level Down"))
            {
                if (buildingManager.MoveFloorDown())
                    MoveCharacterDownOneFloor();
            }
        }
    }

    

    public void MoveCharacterUpOneFloor()
    {
        MoveCharacterBetweenFloors(10);
    }

    public void MoveCharacterDownOneFloor()
    {
        MoveCharacterBetweenFloors(-3);
    }

    // moves the character between floors by a set distance, it will then use a raycast to move the character directly
    // onto the floor
    void MoveCharacterBetweenFloors(float distance)
    {
        // move character vertically
        transform.Translate(0, distance, 0);

        // raycast downwards to find the floor, then position character there
        if (Physics.Raycast(transform.position, -transform.up, out hit, raycastDistance, layerMask))
        {
            if (hit.transform != this.transform)
            {
                transform.SetPositionAndRotation(hit.point, transform.rotation);
            }
        }
    }

    // Sets the cursor lock state, reveals cursor if unlocked
    void SetCursorLockedTo(bool value)
    {
        if(value == true)
            Cursor.lockState = CursorLockMode.Locked;
        else
            Cursor.lockState = CursorLockMode.None;

        Cursor.visible = !value;
    }

    public void InitBuildingMode()
    {
        buildingModeIsOn = true;
        targetIndicator.SetActive(false);
    }

    void ExitBuildingMode()
    {
        buildingModeIsOn = false;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        LockCamera(false);
        GameObject.Destroy(currentPreview);
        selectedObject = null;
        currentPreview = null;
        currentInventoryItem = null;
        SetRemoveToolTo(false);
    }

    void PreviewObject()
    {
        if (selectedObject == null)
            return;

        //hide cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;


        // instantiate a preview version of the selected inventory object
        currentPreview = Instantiate(selectedObject, transform.position, Quaternion.identity) as GameObject;



        if (!removeToolIsActive)
        {
            currentPreview.GetComponent<RemoveMode>().enabled = false;
            currentPreview.GetComponent<PreviewMode>().enabled = true;
        }
        else
            removeToolInstance = currentPreview;
 
        // update Transform data to this new object
        previewTransform = currentPreview.transform;

        
        

        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, raycastDistance, tableSurface))
        {
            if (hit.transform != this.transform)
            {
                PlaceObjectPreview(hit);
            }
        }
    }

    

    void BuildingMode()
    {
        // Do not move preview object while camera is in transition (otherwise the preview will move all over the place following the raycast)
        if (inCamTransition)
            return;

        if (!currentPreview)
            return;

        PositionSelectedObject();

        currentPreview.transform.position = previewPosition; //update the position of the preview with 
        currentPreview.transform.rotation = previewRotation;

        // change the material of the object to PreviewShaders
        ChangeColor();

        // Check if player is pressing Key to place object in position
        if (Input.GetButtonDown("Select") && buildingModeIsOn)
        {
            PlaceObject();
        }
    }

    void LockCamera(bool value)
    {
        //Debug.Log("Camera is locked: " + value);
        fpsCam.isLocked = value;
        birdsEyeCam.isLocked = value;
        
        anim.SetBool("isLocked", value);
        character.isLocked = value;
    }

    // moves the preview version of the object around based on a raycast
    public void PositionSelectedObject()
    {
        if(rotateMode)
        {
            // lock the camera
            //cam.GetComponent<MouseLookBasic>().IsLocked = true;
             
            LockCamera(rotateMode);

            // take mouse horizontal movement 
            float mouseX = Input.GetAxisRaw("Mouse X");
            rotateAmount += mouseX;
            
            // use mouseX to rotate the object. clamp within 360 degrees
            previewRotation = Quaternion.AngleAxis(-rotateAmount, Vector3.up);
        }
        // raycast from camera pos to where it's pointing
        else
        {
            if(placingTableCard || removeToolIsActive)
            {
                if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, raycastDistance, layerMask))
                {
                    // lock the camera
                    //cam.GetComponent<MouseLookBasic>().IsLocked = false;
                    LockCamera(rotateMode);


                    if (hit.transform != this.transform)
                    {
                        PlaceObjectPreview(hit);
                    }
                }

                if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, raycastDistance, tableSurface))
                {
                    // lock the camera
                    //cam.GetComponent<MouseLookBasic>().IsLocked = false;
                    LockCamera(rotateMode);


                    if (hit.transform != this.transform)
                    {
                        PlaceObjectPreview(hit);
                    }
                }

            }
            else if(Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, raycastDistance, layerMask))
            {
                // lock the camera
                //cam.GetComponent<MouseLookBasic>().IsLocked = false;
                LockCamera(rotateMode);
             

                if(hit.transform != this.transform)
                {
                    PlaceObjectPreview(hit);
                }
            }  

        }
    }

    public void RemoveTool()
    {
        Debug.Log("remove tool is doing stuff");
        if(Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, raycastDistance, layerMask))
        {
            string msg = hit.collider.gameObject.name;


            Debug.Log(msg);
            if (hit.collider.gameObject.CompareTag("Inventory Item"))
            {
                Debug.Log("found some inventory");
                
                //color it red
                // then when clicked, delete it from scene and data file

            }
        }
    }
 
    // Place a preview version of the object where the camera is pointed
    void PlaceObjectPreview(RaycastHit hit)
    {        
        ChangeColor();
        previewPosition = hit.point; 
    }

    // Changes the color of the object, determined by a script in the object itself that tracks its collisions with objects on a specific layer
    void ChangeColor()
    {
        if (!currentPreview)
            return;
        // check if the object preview mode script says it's a buildable object
        // buildable means it's not colliding with anything
        // if so make it look green, if not, red
        if (removeToolIsActive)
            return;


        if (currentPreview.GetComponent<PreviewMode>().IsBuildable())
        {
            var mat = currentPreview.GetComponent<Renderer>();
            if(mat)
                mat.material = matGreen;

            foreach(Transform child in currentPreview.transform)
            {
                if (child.GetComponent<Renderer>())
                    child.GetComponent<Renderer>().material = matGreen;
            }

        }
        else 
        {
            var mat = currentPreview.GetComponent<Renderer>();
            if (mat)
                mat.material = matRed;
            
            foreach (Transform child in currentPreview.transform)
            {
                if(child.GetComponent<Renderer>())
                    child.GetComponent<Renderer>().material = matRed;
            }
        }
    }

    // Place object where the preview is located
    void PlaceObject()
    {
        if(removeToolIsActive)
        {
            //Debug.Log("REMOVING!!!");
            GameObject removedItem = removeToolInstance.GetComponentInChildren<RemoveTool>().DeleteSelected();
            if(removedItem)
            {
                // TODO: check if it's a card label
                gm.DeleteInventoryObject(removedItem, buildingManager.GetCurrentFloor());
                GameObject.Destroy(removedItem);
            }
                

        }
        else if (currentPreview != null)
        {
            if (currentPreview.GetComponent <PreviewMode>().IsBuildable())
            {
                GameObject newItem = Instantiate(selectedObject, previewPosition, previewRotation);// Quaternion.identity);     
                newItem.GetComponent<RemoveMode>().enabled = true;

                // check if it's a card level item
                if (currentInventoryItem.name == "Card Label")
                {
                    currentCardLabel = newItem;
                    SetPausePromptTo(true); // pause game
                    

                }
                else
                {
                    gm.CreateNewInventoryObject(currentInventoryItem, previewPosition, previewRotation, buildingManager.GetCurrentFloor()); // save this as an inventory object
                    buildingManager.AddItemToFloor(newItem);
                }
            }
        }
    }


    public GameObject UI_prompt_input;
    GameObject currentCardLabel = null;
    
    bool inputModeIsOn = false;
    public void SetPausePromptTo(bool value)
    {
        isPaused = value;
        SetCursorLockedTo(!value);
        LockCamera(value);
        UI_prompt_input.SetActive(value);
        UI_prompt_input.GetComponent<UI_Prompt_Input_Controller>().SelectMe();
        inputModeIsOn = value;
        
    }


    public void SetCardLabelTo(string msg)
    {        
        currentCardLabel.GetComponent<CardLabelController>().UpdateCardLabelTo(msg);

        // tell gm to save this card
        gm.CreateNewLabel(currentCardLabel.GetComponent<CardLabelController>(), previewPosition, previewRotation, buildingManager.GetCurrentFloor()); // save this as an inventory object
        buildingManager.AddItemToFloor(currentCardLabel);


        currentCardLabel = null;
        SetPausePromptTo(false);
    }

    InventoryItem currentInventoryItem;
    // Changes the object preview to a new item, determined by the build GUI
    public void UpdateSelectedObject(InventoryItem item)
    {
        SetRemoveToolTo(false);
        currentInventoryItem = item;
        InitBuildingMode();
        buildingMenuIsOpen = false;
        //cam.GetComponent<MouseLookBasic>().IsLocked = false;

        // destroy the old preview object before its replaced by new preview
        if(currentPreview)
            GameObject.Destroy(currentPreview);


        selectedObject = item.prefab;

        // if the current item being placed is a table card, make sure to change to layer mask to only table surfaces, otherwise use default layermask
        if (item.name == "Card Label")
            placingTableCard = true;
        else
            placingTableCard = false;


        PreviewObject();
        LockCamera(false);
    }



    public float userSettings_cameraSensitivity = 5.0f;
    public float userSettings_movementSensitivity = 5.0f;
    // Updates player movements settings and saves them to player prefs
    public void UpdateMovementSettings()
    {
        /* Camera Sensitivity Mappings
        f(0) = 200 + (n * 20) = 200
        f(3) = 200 + (n * 20) = 260        
        f(5) = 200 + (n * 20) = 300
        f(10) = 200 + (n * 20)  = 400
        */

        // TODO: camera sensitivity
        // grab value from settings slider
        float cameraSensitivity = 200 + (userSettings_cameraSensitivity * 20);

        // adjust FPS cam 
        cm_fpsCamUnlocked.GetCinemachineComponent<CinemachinePOV>().m_HorizontalAxis.m_MaxSpeed = cameraSensitivity;
        cm_fpsCamUnlocked.GetCinemachineComponent<CinemachinePOV>().m_VerticalAxis.m_MaxSpeed = cameraSensitivity;

        // adjust BIRDS cam
        cm_BirdsEyeCamUnlocked.m_XAxis.m_MaxSpeed = cameraSensitivity;


        // adjust player movement sensitivity
        float newRunSpeed;
        if (userSettings_movementSensitivity > 0)
            newRunSpeed = (userSettings_movementSensitivity + 4);
        else
            newRunSpeed = 1;

        GetComponent<PlayerControllerBasic>().UpdateRunSpeed(newRunSpeed);

        Debug.Log("Saving player prefs");
        // save to unity player prefs (data manager should handle this)
        SavePlayerPrefs();
    } 

    // Settings menu sends values here to update player movement values
    public void ChangePlayerSettings(float playerMovement, float cameraMove)
    {
        Debug.Log("Changing player settings");
        userSettings_cameraSensitivity = cameraMove;
        userSettings_movementSensitivity = playerMovement;
        UpdateMovementSettings();
    }

    void SavePlayerPrefs()
    {
        PlayerPrefs.SetFloat("PlayerMovement", userSettings_movementSensitivity);
        PlayerPrefs.SetFloat("CameraMovement", userSettings_cameraSensitivity);
        PlayerPrefs.Save();
    }

    void GetPlayerPrefs()
    {
        if (!PlayerPrefs.HasKey("PlayerMovement"))
        {
            Debug.Log("Player Prefs: Player movement not found.");
            PlayerPrefs.SetFloat("PlayerMovement", 5.0f);
        }
        
        userSettings_movementSensitivity = PlayerPrefs.GetFloat("PlayerMovement");
        
        if(!PlayerPrefs.HasKey("CameraMovement"))
        {
            Debug.Log("Player Prefs: Camera movement not found.");
            PlayerPrefs.SetFloat("CameraMovement", 5.0f);
        }
        
        userSettings_cameraSensitivity = PlayerPrefs.GetFloat("CameraMovement");

        Debug.Log("Player prefs set to: Player Movement = " + userSettings_movementSensitivity +
            " | Camera movement = " + userSettings_cameraSensitivity);
        UpdateMovementSettings();
    }

    public float GetPlayerPrefs(string name)
    {
        switch(name)
        {
            case "PlayerMovement": return userSettings_movementSensitivity;
            case "CameraMovement": return userSettings_cameraSensitivity;
            default: return -1;
        }
    }
}

}
