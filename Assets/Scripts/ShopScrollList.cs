﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopScrollList : MonoBehaviour
{
    public List<InventoryItem> itemList;

    public Transform contentPanel;
    public ShopScrollList otherShop; // this is how two panels communicate
    public SimpleObjectPool buttonObjectPool;
    

    // Start is called before the first frame update
    void Start()
    {
        RefreshDisplay();
    }


    // go through list, count items, then add button as needed. Call setup function for each button
    void AddButtons()
    {
        // loop through list
        for(int i = 0; i < itemList.Count; i++)
        {
            InventoryItem item = itemList[i];
            GameObject newButton = buttonObjectPool.GetObject();
            newButton.transform.SetParent(contentPanel); // parent to content panel that has horizontal layout, so it's added to that layout correctly

            InventoryButton sampleButton = newButton.GetComponent<InventoryButton>(); // create a new sample button, give it the SampleButton script
            sampleButton.Setup(item, this); // call it's setup function, but pass in the current item and a reference to this scroll list
        }
    }


    public void RefreshDisplay()
    {
        AddButtons();
    }


    void AddItem(InventoryItem itemToAdd, ShopScrollList shopList)
    {
        shopList.itemList.Add(itemToAdd);
    }

    void RemoveItem(InventoryItem itemToRemove, ShopScrollList shopList)
    {
        // count down, bc if you count up and remove items, you mess up the count
        for (int i = shopList.itemList.Count - 1; i > 0; i--)
        {
            if (shopList.itemList[i] == itemToRemove)
            {
                shopList.itemList.RemoveAt(i);
            }
        }
    }



}
