﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLookBasic : MonoBehaviour
{
    public bool IsLocked = false;
    Vector2 mouseLook;
    Vector2 smoothV;
    Vector3 _cameraOffSet;
    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;
    public float rotationSpeed = 5.0f;

    // clamp the camera's y angle to min and max
    public float Y_ANGLE_MIN = 0.0f;
    public float Y_ANGLE_MAX = 50.0f;

    GameObject character;

    // Use this for initialization
    void Start()
    {
        character = this.transform.parent.gameObject;
        _cameraOffSet = transform.position - transform.parent.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (IsLocked)
            return;


        var md = new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y"));
        
        md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
        
        smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
        mouseLook += smoothV;

        mouseLook.y = Mathf.Clamp(mouseLook.y, Y_ANGLE_MIN, Y_ANGLE_MAX);
        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);
      
    }
}

