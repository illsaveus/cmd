﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Camera cam;
    public GameObject character;
    public bool isLocked = false;
    public bool isActive = true;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(isLocked)
            return;
        else if (isActive)
            transform.rotation = cam.transform.rotation;

        
        // Rotate the character by the same amount the camera is rotated
        //character.transform.rotation = Quaternion.Euler(new Vector3(0, cam.transform.rotation.eulerAngles.y, 0));
    }
 

}
