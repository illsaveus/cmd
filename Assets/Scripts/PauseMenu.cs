﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PauseMenu : MonoBehaviour
{
    public GameObject PauseMenuPanel, RenameEventPanel, ControlsMenuPanel;
    public GameController gm;

    public TMP_InputField input_rename;
    public TMP_Text ui_eventTitle;

    // Start is called before the first frame update
    void Start()
    {
        var finder = GameObject.FindGameObjectWithTag("GameController");
        if (finder)
            gm = finder.GetComponent<GameController>();

        
    } 

    public void RenameEvent()
    {
        gm.RenameCurrentEvent(input_rename.text);
        ui_eventTitle.text = gm.GetEventName();
    }

    public void ActivatePauseMenu()
    {
        if (!gm)
            return;

        ui_eventTitle.text = gm.GetEventName();
        PauseMenuPanel.SetActive(true);
        RenameEventPanel.SetActive(false);
        ControlsMenuPanel.SetActive(false);
    }

    public void ActivateRenameEventPanel()
    {
        if (!gm)
            return;

        PauseMenuPanel.SetActive(false);
        RenameEventPanel.SetActive(true);

        input_rename.text = gm.GetEventName();
    }

    public void ActivateControlsMenu()
    {
        PauseMenuPanel.SetActive(false);
        ControlsMenuPanel.SetActive(true);
    }

    

}
