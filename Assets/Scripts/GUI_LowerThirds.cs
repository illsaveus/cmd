﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TBB;
public class GUI_LowerThirds : MonoBehaviour
{
    public GameObject[] tabButtons,tabMenus;

    Animator anim;
    public DragAndDrop buildMode;

    private void Start()
    {
        anim = GetComponent<Animator>();
        
    }

    public void Activate()
    {
        anim.SetTrigger("Activate");
    }

    public void Deactivate()
    {
        anim.SetTrigger("Deactivate");
    }

    public void UpdatePreviewItem(InventoryItem newItem)
    {
        buildMode.UpdateSelectedObject(newItem);
        Deactivate();
    }

    public void ShowBuildMenu()
    {
        tabMenus[1].SetActive(true);
        tabButtons[1].GetComponent<UI_Button_Selector>().Selected(true);

        tabMenus[0].SetActive(false);
        tabButtons[0].GetComponent<UI_Button_Selector>().Selected(false);
    }

    public void ShowOptionsMenu()
    {
        tabMenus[0].SetActive(true);
        tabButtons[0].GetComponent<UI_Button_Selector>().Selected(true);

        tabMenus[1].SetActive(false);
        tabButtons[1].GetComponent<UI_Button_Selector>().Selected(false);

    }
}
